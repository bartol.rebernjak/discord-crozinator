const Discord = require('discord.js');
const client = new Discord.Client();

token = '';

josipResponses = [
	'Josip please stop contacting me',
	'šuti josip',
	'joko nemoj davit'
]

var minute;
var imminentTimeout;

imminentHranaAlert = function(channel)	{
	channel.send('@everyone HRANA ZA 5 MINUTA');
}

clearMinute = function()	{
	minute = null;
}

function hrana(msg)	{
	if (msg.content.split(' ').length == 2)	{
		if (minute)	{
			return 'već brojim minute';
		}
		minute = parseInt(msg.content.split(' ')[1]);
		end = Math.floor(Date.now() / 60000) + minute;
		setTimeout(clearMinute, minute * 60000);
		if (minute >= 5)	{
			imminentTimeout = setTimeout(imminentHranaAlert, minute * 60000 - 300000, msg.channel);
		}
		return 'ok gazda';
	}	else	{
		if (minute)	{
			return 'još ' + (end - Math.floor(Date.now() / 60000)) + ' minuta'
		}	else	{
			return 'ne brojim ništa';
		}
	}
}

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', msg => {
  if (msg.content === '!kju') {
    msg.channel.send('!queue');
  }
  if (msg.content === '!kokopelli') {
    msg.channel.send(':hole:');
  }
  if (msg.content === '!odjebi') {
	clearTimeout(imminentTimeout);
	minute = null;
    msg.channel.send('jesm šefe');
  }
  if (msg.content.substring(0, 6) === '!hrana') {
    msg.channel.send(hrana(msg));
  }
  if (Math.random() > 0.9 &&(msg.author.username === 'joshko' || msg.member.nickname && (msg.member.nickname.toLowerCase() === 'koko' || msg.member.nickname.toLowerCase() === 'joko'))) {
    msg.channel.send(josipResponses[Math.floor(Math.random()*josipResponses.length)]);
  }
});

client.login(token);